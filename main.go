package main

import (
	"./service"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"os"
	"time"
)

const (
	DefaultConfigurationFilePath = "conf.json"
)

func main() {
	// configuration file path
	configurationFilePath := DefaultConfigurationFilePath
	if len(os.Args) > 1 {
		configurationFilePath = os.Args[1]
	}

	if configuration, err := service.BuildConfiguration(configurationFilePath); err == nil {
		// build service
		if srv, err := service.BuildService(common.GhostService[common.ServiceControllerHUE].DefaultPort,
			configuration); err == nil {
			// update
			for {
				// update service
				srv.Update()

				// wait
				time.Sleep(time.Second)
			}
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		fmt.Println(err)
		os.Exit(1)
	}

}
