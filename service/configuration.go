package service

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

type Configuration struct {
	// security manager
	SecurityManager struct {
		Hostname string `json:"hostname"`
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"securitymanager"`

	// directory
	Directory struct {
		Hostname string `json:"hostname"`
	} `json:"directory"`

	// scanner
	Scanner struct {
		Hostname string `json:"hostname"`
	} `json:"scanner"`

	// rabbit mq
	RabbitMQ struct {
		Hostname []string `json:"hostname"`
		Username string
		Password string
	} `json:"rabbitmq"`

	// api key directory
	ApiKeyDirectory string `json:"apiKeyDirectory"`
}

// build configuration
func BuildConfiguration(configurationFilePath string) (*Configuration, error) {
	// read configuration file
	if content, err := ioutil.ReadFile(configurationFilePath); err == nil {
		// configuration instance
		var configuration Configuration

		// parse json
		if err := json.Unmarshal(content,
			&configuration); err == nil {
			configuration.ApiKeyDirectory = strings.TrimSuffix( configuration.ApiKeyDirectory,
				"/" ) + "/"
			configuration.ApiKeyDirectory = os.ExpandEnv(configuration.ApiKeyDirectory)
			_ = os.MkdirAll( configuration.ApiKeyDirectory,
				777 )
			return &configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
