package service

import (
	"bytes"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"net/http"
)

const (
	HueDimmerSensorTypeName = "ZLLSwitch"
)

func ChangeSwitchName(sw *device.Switch,
	address string,
	apiKey string,
	newName string) {
	request, _ := http.NewRequest("PUT",
		"https://"+
			address+
			"/api/"+
			apiKey+
			"/sensors/"+
			sw.Id,
		bytes.NewBuffer([]byte("{\"name\":\""+
			newName+
			"\"}")))
	common.DiscardHTTPResponse(common.InsecureHTTPClient.Do(request))
}
