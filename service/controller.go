package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	DelayBetweenHUEUpdate = time.Millisecond * 300
)

type Controller struct {
	// rabbitmq controller
	rabbitMQController *rabbit.Controller

	// api key directory
	apiKeyDirectory string

	// ip
	address string

	// config
	config hueApiConfig

	// api key
	apiKey string

	// is alive
	isAlive bool

	// lights list
	Light map[string]*device.Light

	// sensor list
	Switch map[string]*device.Switch

	// mutex
	sync.Mutex
}

// build controller
func BuildController(ip string,
	apiKeyDirectory string,
	rabbitMQController *rabbit.Controller) *Controller {
	// allocate
	controller := &Controller{
		rabbitMQController: rabbitMQController,
		address:            ip,
		isAlive:            true,
		Light:              make(map[string]*device.Light),
		Switch:             make(map[string]*device.Switch),
	}

	// save api key directory
	controller.apiKeyDirectory = strings.TrimRight(apiKeyDirectory,
		"/")
	controller.apiKeyDirectory += "/"

	// run thread
	go controller.updateThread()

	// done
	return controller
}

// update
func (controller *Controller) update() {
	// is dead
	isDead := false

	// do we have a uuid
	if controller.config.UUID == "" {
		// request uuid
		request, _ := http.NewRequest("GET",
			"http://"+
				controller.address+
				"/api/config",
			nil)
		if response, err := http.DefaultClient.Do(request); err == nil {
			if content, err := ioutil.ReadAll(response.Body); err == nil {
				_ = response.Body.Close()
				if response.StatusCode == http.StatusOK {
					if err = json.Unmarshal(content,
						&controller.config); err == nil {
						// notify
						fmt.Println("hub at",
							controller.address,
							"has uuid",
							controller.config.UUID)

						// try to load api key
						if err := controller.loadApiKey(); err == nil {
							fmt.Println("loaded api key for",
								controller.address,
								"now testing if valid...")
							request, _ = http.NewRequest("GET",
								"https://"+
									controller.address+
									"/api/"+
									controller.apiKey+
									"/lights",
								nil)
							if response, err := common.InsecureHTTPClient.Do(request); err == nil {
								if content, err = ioutil.ReadAll(response.Body); err == nil {
									_ = response.Body.Close()
									if strings.Contains(string(content),
										"unauthorized user") {
										controller.removeApiKey()
										fmt.Println("api key for",
											controller.address,
											"is INVALID! now renewing...")
									} else {
										fmt.Println("api key for",
											controller.address,
											"is valid")
									}
								} else {
									_ = response.Body.Close()
									isDead = true
								}
							} else {
								isDead = true
							}
						} else {
							fmt.Println("couldn't find api key for",
								controller.address,
								"please press hue hub button")
						}
					} else {
						isDead = true
					}
				} else {
					isDead = true
				}
			} else {
				_ = response.Body.Close()
				isDead = true
			}
		} else {
			isDead = true
		}
	} else {
		if controller.apiKey == "" {
			// request an api key
			request, _ := http.NewRequest("POST",
				"https://"+
					controller.address+
					"/api",
				bytes.NewBuffer([]byte("{\"devicetype\":\"ghostbutler\"}")))
			if response, err := common.InsecureHTTPClient.Do(request); err == nil {
				content, err := ioutil.ReadAll(response.Body)
				_ = response.Body.Close()
				if response.StatusCode == http.StatusOK {
					// trim content
					stringContent := strings.TrimLeft(string(content),
						"[")
					stringContent = strings.TrimRight(stringContent,
						"]")

					// parse
					var auth hueAuthentication
					if err = json.Unmarshal([]byte(stringContent),
						&auth); err == nil {
						if auth.Success.Username != "" {
							controller.apiKey = auth.Success.Username
							if err = controller.saveApiKey(); err != nil {
								fmt.Println("couldn't save api key for",
									controller.address,
									":",
									err)
							}
						} else {
							fmt.Println("please press button for hue at",
								controller.address)
						}
					} else {
						isDead = true
					}
				} else {
					isDead = true
				}
			} else {
				isDead = true
			}
		} else {
			// request light status
			request, _ := http.NewRequest("GET",
				"https://"+
					controller.address+
					"/api/"+
					controller.apiKey+
					"/lights",
				nil)
			if response, err := common.InsecureHTTPClient.Do(request); err == nil {
				content, _ := ioutil.ReadAll(response.Body)
				_ = response.Body.Close()
				var lightList map[string]hueLight
				if err = json.Unmarshal(content,
					&lightList); err == nil {
					// iterate lights
					for id, light := range lightList {
						// allocate
						newLight := &device.Light{
							Name: light.Name,
							Type: device.LightTypeName[light.ModelId],
							UUID: light.UUID,
							State: device.LightState{
								Brightness:  light.State.Brightness,
								HUE:         light.State.Hue,
								IsOn:        light.State.On,
								IsReachable: light.State.Reachable,
								Saturation:  light.State.Saturation,
							},
							Id: id,
						}

						// save light
						controller.Lock()
						if oldLight, ok := controller.Light[newLight.UUID]; ok {
							if IsDifferent(oldLight,
								newLight) {
								newLight.IsMustSave = true
							}
						} else {
							newLight.IsMustSave = true
						}
						controller.Light[newLight.UUID] = newLight
						controller.Unlock()
					}
				} else {
					isDead = true
				}
			} else {
				isDead = true
			}

			// request sensors (remote)
			request, _ = http.NewRequest("GET",
				"https://"+
					controller.address+
					"/api/"+
					controller.apiKey+
					"/sensors",
				nil)
			if response, err := common.InsecureHTTPClient.Do(request); err == nil {
				content, _ := ioutil.ReadAll(response.Body)
				_ = response.Body.Close()
				var sensorList map[string]hueSensor
				if err = json.Unmarshal(content,
					&sensorList); err == nil {
					for id, sensor := range sensorList {
						switch sensor.Type {
						case HueDimmerSensorTypeName:
							// extract status
							var status device.ButtonStatus
							var name string
							switch sensor.State.Button % 1000 {
							case 0:
								status = device.ButtonStatusPressed
								break
							case 1:
								status = device.ButtonStatusLongPressed
								break
							case 2:
								status = device.ButtonStatusReleased
								break
							case 3:
								status = device.ButtonStatusLongReleased
								break

							default:
								continue
							}
							switch sensor.State.Button / 1000 {
							case 1:
								name = strings.TrimRight(sensor.Name,
									" ") + ".button_on"
								break
							case 2:
								name = strings.TrimRight(sensor.Name,
									" ") + ".button_up"
								break
							case 3:
								name = strings.TrimRight(sensor.Name,
									" ") + ".button_down"
								break
							case 4:
								name = strings.TrimRight(sensor.Name,
									" ") + ".button_off"
								break

							default:
								name = strings.TrimRight(sensor.Name,
									" ") + ".button_unknown"
								break
							}

							// build switch
							sw := &device.Switch{
								Id:   id,
								Name: sensor.Name,
								Event: device.Button{
									Index:  sensor.State.Button / 1000,
									Status: status,
									Name:   name,
								},
								Type:        device.SwitchEventTypeButton,
								IsReachable: sensor.Config.IsReachable,
								LastUpdate:  sensor.State.LastUpdate,
								UUID:        sensor.UUID,
							}

							// update switch
							controller.Lock()
							if oldSw, ok := controller.Switch[sensor.UUID]; ok {
								if oldSw.LastUpdate != sw.LastUpdate ||
									oldSw.Name != sw.Name {
									sw.IsMustSave = true
								}
							} else {
								sw.IsMustSave = true
							}
							controller.Switch[sensor.UUID] = sw
							controller.Unlock()
							break

						default:
							break
						}
					}
				} else {
					isDead = true
				}
			} else {
				isDead = true
			}
		}
	}

	// check alive
	if isDead {
		controller.isAlive = false
		fmt.Println("controller for",
			controller.address,
			"is now dead")

		// all sensors unreachable
		for _, light := range controller.Light {
			light.State.IsReachable = false
			light.IsMustSave = true
		}
		for _, sw := range controller.Switch {
			sw.IsReachable = false
			sw.IsMustSave = true
		}
	}

	// save modifications
	controller.Lock()
	for _, light := range controller.Light {
		if light.IsMustSave {
			if channel := controller.rabbitMQController.Channel; channel != nil {
				_ = channel.Publish("",
					rabbit.SensorQueueName,
					false,
					false,
					amqp.Publishing{
						ContentType: "application/json",
						Body:        light.MarshalJson(),
					})
			}
		}
	}
	for _, sw := range controller.Switch {
		if sw.IsMustSave {
			if channel := controller.rabbitMQController.Channel; channel != nil {
				swCopy := device.Switch{
					Type:        sw.Type,
					UUID:        sw.UUID,
					IsReachable: sw.IsReachable,
					LastUpdate:  sw.LastUpdate,
					Id:          sw.Id,
					Event:       sw.Event,
				}
				switch sw.Event.(type) {
				case device.Button:
					swCopy.Name = sw.Event.(device.Button).Name
					break
				case device.Joystick:
					swCopy.Name = sw.Event.(device.Joystick).Name
					break

				default:
					swCopy.Name = sw.Name
					break
				}

				_ = channel.Publish("",
					rabbit.SensorQueueName,
					false,
					false,
					amqp.Publishing{
						ContentType: "application/json",
						Body:        swCopy.MarshalJson(),
					})
			}
		}
	}
	controller.Unlock()
}

// update thread
func (controller *Controller) updateThread() {
	// is controller running?
	for controller.isAlive {
		controller.update()
		time.Sleep(DelayBetweenHUEUpdate)
	}
}

// save api key
func (controller *Controller) saveApiKey() error {
	// build file path
	filePath := controller.apiKeyDirectory +
		controller.config.UUID

	// write key
	return ioutil.WriteFile(filePath,
		[]byte(controller.apiKey),
		0644)
}

// load api key
func (controller *Controller) loadApiKey() error {
	// build file path
	filePath := controller.apiKeyDirectory +
		controller.config.UUID

	// read file content
	if content, err := ioutil.ReadFile(filePath); err == nil {
		controller.apiKey = string(content)
		return nil
	} else {
		return err
	}
}

// remove api key
func (controller *Controller) removeApiKey() {
	// build file path
	filePath := controller.apiKeyDirectory +
		controller.config.UUID

	// remove
	_ = os.Remove(filePath)

	// forget
	controller.apiKey = ""
}

// extract lights list
func (controller *Controller) ExtractLightNameList() []string {
	// output
	output := make([]string,
		0,
		1)

	// lock
	controller.Lock()
	defer controller.Unlock()

	// iterate lights
	for _, light := range controller.Light {
		output = append(output,
			light.Name)
	}

	// done
	return output
}

// extract buttons list
func (controller *Controller) ExtractButtonNameList() []string {
	// output
	output := make([]string,
		0,
		1)

	// lock
	controller.Lock()
	defer controller.Unlock()

	// iterate lights
	for _, sw := range controller.Switch {
		for _, button := range []string{".button_on", ".button_off", ".button_up", ".button_down"} {
			output = append(output,
				strings.TrimRight(sw.Name,
					" ")+
					button)
		}
	}

	// done
	return output
}
