package service

type hueApiConfig struct {
	// name
	Name string `json:"name"`

	// api version
	Version string `json:"apiversion"`

	// hub mac address
	Mac string `json:"mac"`

	// hub uuid
	UUID string `json:"bridgeid"`
}

type hueAuthentication struct {
	Error struct {
		Type        int    `json:"type"`
		Address     string `json:"address"`
		Description string `json:"description"`
	} `json:"error"`
	Success struct {
		Username string `json:"username"`
	} `json:"success"`
}

type hueLight struct {
	State struct {
		On         bool   `json:"on"`
		Brightness int    `json:"bri"`
		Hue        int    `json:"hue"`
		Saturation int    `json:"sat"`
		Effect     string `json:"effect"`
		Reachable  bool   `json:"reachable"`
	} `json:"state"`

	Name    string `json:"name"`
	ModelId string `json:"modelid"`
	UUID    string `json:"uniqueid"`
}

type hueSensor struct {
	State struct {
		//DayLight string `json:"daylight"`
		Button     int    `json:"buttonevent"`
		LastUpdate string `json:"lastupdated"`
	} `json:"state"`

	Config struct {
		IsOn        string `isOn:"on"`
		IsReachable bool   `json:"reachable"`
	} `json:"config"`

	Name    string `json:"name"`
	Type    string `json:"type"`
	ModelId string `json:"modelid"`
	UUID    string `json:"uniqueid"`
}
