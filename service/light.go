package service

import (
	"bytes"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"image/color"
	"math"
	"net/http"
	"strconv"
)

func LightOn(light *device.Light,
	address string,
	apiKey string) {
	request, _ := http.NewRequest("PUT",
		"https://"+
			address+
			"/api/"+
			apiKey+
			"/lights/"+
			light.Id+
			"/state",
		bytes.NewBuffer([]byte("{\"on\":true}")))
	common.DiscardHTTPResponse(common.InsecureHTTPClient.Do(request))
}

func LightOff(light *device.Light,
	address string,
	apiKey string) {
	request, _ := http.NewRequest("PUT",
		"https://"+
			address+
			"/api/"+
			apiKey+
			"/lights/"+
			light.Id+
			"/state",
		bytes.NewBuffer([]byte("{\"on\":false}")))
	common.DiscardHTTPResponse(common.InsecureHTTPClient.Do(request))
}

func convertRGBToXY(color color.RGBA) (x, y float64) {
	normalizedToOne := make([]float64,
		3)
	var cred, cgreen, cblue float64
	cred = float64(color.R)
	cgreen = float64(color.G)
	cblue = float64(color.B)
	normalizedToOne[0] = cred / 255
	normalizedToOne[1] = cgreen / 255
	normalizedToOne[2] = cblue / 255
	var red, green, blue float64

	// Make red more vivid
	if normalizedToOne[0] > 0.04045 {
		red = math.Pow(
			(normalizedToOne[0]+0.055)/(1.0+0.055), 2.4)
	} else {
		red = normalizedToOne[0] / 12.92
	}

	// Make green more vivid
	if normalizedToOne[1] > 0.04045 {
		green = math.Pow((normalizedToOne[1]+0.055)/(1.0+0.055), 2.4)
	} else {
		green = normalizedToOne[1] / 12.92
	}

	// Make blue more vivid
	if normalizedToOne[2] > 0.04045 {
		blue = math.Pow((normalizedToOne[2]+0.055)/(1.0+0.055), 2.4)
	} else {
		blue = normalizedToOne[2] / 12.92
	}

	X, Y, Z := red*0.649926+green*0.103455+blue*0.197109,
		red*0.234327+green*0.743075+blue*0.022598,
		red*0.0000000+green*0.053077+blue*1.035763

	x = X / (X + Y + Z)
	y = Y / (X + Y + Z)

	return
}

func convertRGBToBrightness(color color.RGBA) uint8 {
	max := math.Max(math.Max(float64(color.R), float64(color.G)),
		float64(color.B))
	return uint8(max)
}

func LightColorRGB(light *device.Light,
	address string,
	apiKey string,
	color color.RGBA) {
	x, y := convertRGBToXY(color)
	request, _ := http.NewRequest("PUT",
		"https://"+
			address+
			"/api/"+
			apiKey+
			"/lights/"+
			light.Id+
			"/state",
		bytes.NewBuffer([]byte("{\"xy\":["+
			strconv.FormatFloat(x,
				'e',
				3,
				64)+
			","+
			strconv.FormatFloat(y,
				'e',
				3,
				64)+
			"],\"bri\":"+
			strconv.Itoa(int(convertRGBToBrightness(color)))+
			"}")))
	common.DiscardHTTPResponse(common.InsecureHTTPClient.Do(request))
}

func LightColorHueSaturation(light *device.Light,
	address string,
	apiKey string,
	hue int,
	saturation uint8) {
	request, _ := http.NewRequest("PUT",
		"https://"+
			address+
			"/api/"+
			apiKey+
			"/lights/"+
			light.Id+
			"/state",
		bytes.NewBuffer([]byte("{\"hue\":"+
			strconv.Itoa(hue*182)+
			",\"sat\":"+
			strconv.Itoa(int(saturation))+
			"}")))
	common.DiscardHTTPResponse(common.InsecureHTTPClient.Do(request))
}

func LightBlink(light *device.Light,
	address string,
	apiKey string) {
	request, _ := http.NewRequest("PUT",
		"https://"+
			address+
			"/api/"+
			apiKey+
			"/lights/"+
			light.Id+
			"/state",
		bytes.NewBuffer([]byte("{\"alert\":\"select\"}")))
	common.DiscardHTTPResponse(common.InsecureHTTPClient.Do(request))
}

func LightBrightness(light *device.Light,
	address string,
	apiKey string,
	brightness uint8) {
	request, _ := http.NewRequest("PUT",
		"https://"+
			address+
			"/api/"+
			apiKey+
			"/lights/"+
			light.Id+
			"/state",
		bytes.NewBuffer([]byte("{\"bri\":"+
			strconv.Itoa(int(brightness))+
			"}")))
	common.DiscardHTTPResponse(common.InsecureHTTPClient.Do(request))
}

func LightName(light *device.Light,
	address string,
	apiKey string,
	newName string) {
	request, _ := http.NewRequest("PUT",
		"https://"+
			address+
			"/api/"+
			apiKey+
			"/lights/"+
			light.Id,
		bytes.NewBuffer([]byte("{\"name\":\""+
			newName+
			"\"}")))
	common.DiscardHTTPResponse(common.InsecureHTTPClient.Do(request))
}

// is different
func IsDifferent(light *device.Light,
	newLight *device.Light) bool {
	return light.Name != newLight.Name ||
		light.State.IsReachable != newLight.State.IsReachable ||
		light.State.Brightness != newLight.State.Brightness ||
		light.State.HUE != newLight.State.HUE ||
		light.State.IsOn != newLight.State.IsOn ||
		light.State.Saturation != newLight.State.Saturation
}
