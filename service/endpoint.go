package service

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"image/color"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

const (
	APIServiceV1HUEControllerAddDevice = common.APIServiceType(iota + common.APIServiceBuiltInLast)
	APIServiceV1HUEControllerLightOn
	APIServiceV1HUEControllerLightOff
	APIServiceV1HUEControllerLightChangeColor
	APIServiceV1HUEControllerLightChangeBrightness
	APIServiceV1HUEControllerLightBlink
	APIServiceV1HUEControllerLightChangeName
	APIServiceV1HUEControllerLightList
	APIServiceV1HUEControllerLightListName

	APIServiceV1HUEControllerButtonListName
	APIServiceV1HUEControllerButtonChangeName
)

var APIService = map[common.APIServiceType]*common.APIEndpoint{
	APIServiceV1HUEControllerAddDevice: {
		Path:                    []string{"api", "v1", "controller", "add"},
		Method:                  "POST",
		Description:             "Add a new found device to MFI controller",
		Callback:                controllerAddNewDeviceCallback,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1HUEControllerLightList: {
		Path:                    []string{"api", "v1", "controller", "lights"},
		Method:                  "GET",
		Description:             "List all lights",
		Callback:                controllerListAllLight,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1HUEControllerLightListName: {
		Path:                    []string{"api", "v1", "controller", "lights", "list"},
		Method:                  "GET",
		Description:             "List all lights names",
		Callback:                controllerListLightName,
		IsMustProvideOneTimeKey: true,
	},

	APIServiceV1HUEControllerLightOn: {
		Path:                    []string{"api", "v1", "controller", "lights", "on"},
		Method:                  "PUT",
		Description:             "Light on (name)",
		Callback:                controllerLightOn,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1HUEControllerLightOff: {
		Path:                    []string{"api", "v1", "controller", "lights", "off"},
		Method:                  "PUT",
		Description:             "Light off (name)",
		Callback:                controllerLightOff,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1HUEControllerLightChangeColor: {
		Path:                    []string{"api", "v1", "controller", "lights", "color"},
		Method:                  "PUT",
		Description:             "Change light color (name,[hue(0-360),saturation(0-255),brightness(0-255)]/[r,g,b](0-255))",
		Callback:                controllerLightChangeColor,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1HUEControllerLightChangeBrightness: {
		Path:                    []string{"api", "v1", "controller", "lights", "brightness"},
		Method:                  "PUT",
		Description:             "Change light color (name,brightness[0-255])",
		Callback:                controllerLightChangeBrightness,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1HUEControllerLightBlink: {
		Path:                    []string{"api", "v1", "controller", "lights", "blink"},
		Method:                  "PUT",
		Description:             "Blink a light (name)",
		Callback:                controllerLightBlink,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1HUEControllerLightChangeName: {
		Path:                    []string{"api", "v1", "controller", "lights", "name"},
		Method:                  "PUT",
		Description:             "Change light name (name,newName)",
		Callback:                controllerLightChangeName,
		IsMustProvideOneTimeKey: true,
	},

	APIServiceV1HUEControllerButtonListName: {
		Path:                    []string{"api", "v1", "controller", "gamepads", "list"},
		Method:                  "GET",
		Description:             "Get buttons list",
		Callback:                controllerButtonList,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1HUEControllerButtonChangeName: {
		Path:                    []string{"api", "v1", "controller", "gamepads", "name"},
		Method:                  "PUT",
		Description:             "Change button name (name,newName)",
		Callback:                controllerButtonChangeName,
		IsMustProvideOneTimeKey: true,
	},
}

func controllerAddNewDeviceCallback(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)

	// check key
	if body, err := ioutil.ReadAll(request.Body); err == nil {
		var resultList common.ScannerResultList
		if err := json.Unmarshal(body,
			&resultList); err == nil {
			for _, result := range resultList.Result {
				if _, ok := srv.controller[result.IP]; !ok {
					srv.Lock()
					srv.controller[result.IP] = BuildController(result.IP,
						srv.configuration.ApiKeyDirectory,
						srv.rabbitMQController)
					fmt.Println("just added new controller for hue at",
						result.IP)
					srv.Unlock()
				}
			}
			return http.StatusOK
		} else {
			rw.WriteHeader( http.StatusBadRequest )
			_, _ = rw.Write( [ ]byte( "{\"error\":\"bad json\"}" ) )
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader( http.StatusBadRequest )
		return http.StatusBadRequest
	}
}

func controllerListAllLight(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)

	// light list
	lightList := make([]*device.Light,
		0,
		1)

	// lock
	srv.Lock()

	// iterate light controllers
	for _, controller := range srv.controller {
		controller.Lock()
		for _, light := range controller.Light {
			lightList = append(lightList,
				light)
		}
		controller.Unlock()
	}

	// unlock
	srv.Unlock()

	// marshal
	content, _ := json.Marshal(lightList)

	// send
	_, _ = rw.Write(content)
	return http.StatusOK
}

func controllerListLightName(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)

	// total light list
	totalLightList := make([]string,
		0,
		1)

	// lock
	srv.Lock()

	// iterate controllers
	for _, controller := range srv.controller {
		totalLightList = append(totalLightList,
			controller.ExtractLightNameList()...)
	}

	// unlock
	srv.Unlock()

	// marshal json
	content, _ := json.Marshal(totalLightList)

	// send
	_, _ = rw.Write(content)
	return http.StatusOK
}

func controllerLightOn(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// parse form
	if err := request.ParseForm(); err == nil {
		// extract name
		name := common.ExtractFormValue(request,
			"name")

		// lock
		srv.Lock()
		defer srv.Unlock()

		// look for light
		for _, controller := range srv.controller {
			// lock
			controller.Lock()

			// iterate lights
			for _, light := range controller.Light {
				isSelect := false
				if name == "" {
					isSelect = true
				} else {
					if light.Name == name {
						isSelect = true
					}
				}
				if isSelect {
					go LightOn(light,
						controller.address,
						controller.apiKey)
				}
			}

			// unlock
			controller.Unlock()
		}
		return http.StatusOK
	} else {
		rw.WriteHeader( http.StatusBadRequest )
		_, _ = rw.Write( [ ]byte( "{\"error\":\"bad form\"}" ) )
		return http.StatusBadRequest
	}
}

func controllerLightOff(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// parse
	if err := request.ParseForm(); err == nil {

		// extract name
		name := common.ExtractFormValue(request,
			"name")

		// lock
		srv.Lock()
		defer srv.Unlock()

		// look for light
		for _, controller := range srv.controller {
			// lock
			controller.Lock()

			// iterate lights
			for _, light := range controller.Light {
				isSelect := false
				if name == "" {
					isSelect = true
				} else {
					if light.Name == name {
						isSelect = true
					}
				}
				if isSelect {
					go LightOff(light,
						controller.address,
						controller.apiKey)
				}
			}

			// unlock
			controller.Unlock()
		}
		return http.StatusOK
	} else {
		rw.WriteHeader( http.StatusBadRequest )
		_, _ = rw.Write( [ ]byte( "{\"error\":\"bad form\"}" ) )
		return http.StatusBadRequest
	}
}

func controllerLightChangeColor(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// parse
	if err := request.ParseForm(); err == nil {
		// extract name
		name := common.ExtractFormValue(request,
			"name")

		// extract color
		r := common.ExtractFormValue(request,
			"r")
		g := common.ExtractFormValue(request,
			"g")
		b := common.ExtractFormValue(request,
			"b")
		hue := common.ExtractFormValue(request,
			"hue")
		saturation := common.ExtractFormValue(request,
			"saturation")

		// lock
		srv.Lock()
		defer srv.Unlock()

		// check parameter
		if len(r) > 0 &&
			len(g) > 0 &&
			len(b) > 0 {
			// convert to string
			finalR, _ := strconv.Atoi(r)
			finalG, _ := strconv.Atoi(g)
			finalB, _ := strconv.Atoi(b)

			// look for light
			for _, controller := range srv.controller {
				// lock
				controller.Lock()

				// iterate lights
				for _, light := range controller.Light {
					isSelect := false
					if name == "" {
						isSelect = true
					} else {
						if light.Name == name {
							isSelect = true
						}
					}
					if isSelect {
						go LightColorRGB(light,
							controller.address,
							controller.apiKey,
							color.RGBA{R: uint8(finalR), G: uint8(finalG), B: uint8(finalB)})
					}
				}

				// unlock
				controller.Unlock()
			}
		} else if len(hue) > 0 &&
			len(saturation) > 0 {
			// convert to string
			finalHue, _ := strconv.Atoi(hue)
			finalSaturation, _ := strconv.Atoi(saturation)

			// look for light
			for _, controller := range srv.controller {
				// lock
				controller.Lock()

				// iterate lights
				for _, light := range controller.Light {
					isSelect := false
					if name == "" {
						isSelect = true
					} else {
						if light.Name == name {
							isSelect = true
						}
					}
					if isSelect {
						go LightColorHueSaturation(light,
							controller.address,
							controller.apiKey,
							finalHue,
							uint8(finalSaturation))
					}
				}

				// unlock
				controller.Unlock()
			}
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte( "{\"error\":\"missing parameters (r/g/b or hue/saturation)\"}"))
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader( http.StatusBadRequest )
		_, _ = rw.Write( [ ]byte( "{\"error\":\"bad form\"}" ) )
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func controllerLightChangeBrightness(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// parse
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		if brightness := common.ExtractFormValue(request,
			"brightness"); len(brightness) > 0 {
			finalBrightness, _ := strconv.Atoi(brightness)
			// lock
			srv.Lock()
			defer srv.Unlock()

			// look for light
			for _, controller := range srv.controller {
				// lock
				controller.Lock()

				// iterate lights
				for _, light := range controller.Light {
					isSelect := false
					if name == "" {
						isSelect = true
					} else {
						if light.Name == name {
							isSelect = true
						}
					}
					if isSelect {
						go LightBrightness(light,
							controller.address,
							controller.apiKey,
							uint8(finalBrightness))
					}
				}

				// unlock
				controller.Unlock()
			}
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write( [ ]byte( "{\"error\":\"invalid brightness field\"}" ) )
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader( http.StatusBadRequest )
		_, _ = rw.Write( [ ]byte( "{\"error\":\"bad form\"}" ) )
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func controllerLightBlink(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		srv.Lock()
		defer srv.Unlock()
		for _, controller := range srv.controller {
			controller.Lock()
			for _, light := range controller.Light {
				isSelect := false
				if name == "" {
					isSelect = true
				} else {
					if light.Name == name {
						isSelect = true
					}
				}
				if isSelect {
					if channel := srv.rabbitMQController.Channel; channel != nil {
						_ = channel.Publish("",
							rabbit.SensorQueueName,
							false,
							false,
							amqp.Publishing{
								ContentType: "application/json",
								Body:        light.MarshalJson(),
							})
					}
					go LightBlink(light,
						controller.address,
						controller.apiKey)
				}
			}
			controller.Unlock()
		}
		return http.StatusOK
	} else {
		rw.WriteHeader( http.StatusBadRequest )
		_, _ = rw.Write( [ ]byte( "{\"error\":\"bad form\"}" ) )
		return http.StatusBadRequest
	}
}

func controllerLightChangeName(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// parse
	if err := request.ParseForm(); err == nil {
		if name := common.ExtractFormValue(request,
			"name"); len( name ) <= 0 {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte( "{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			if newName := common.ExtractFormValue(request,
				"newName"); len(newName) <= 0 {
				rw.WriteHeader(http.StatusBadRequest)
				_, _ = rw.Write([]byte( "{\"error\":\"newName is empty\"}"))
				return http.StatusBadRequest
			} else {
				// lock
				srv.Lock()
				defer srv.Unlock()

				// look for light
				for _, controller := range srv.controller {
					// lock
					controller.Lock()

					// iterate lights
					for _, light := range controller.Light {
						isSelect := false
						if name == "" {
							isSelect = false
						} else {
							if light.Name == name {
								isSelect = true
							}
						}
						if isSelect {
							go LightName(light,
								controller.address,
								controller.apiKey,
								newName)
							break
						}
					}

					// unlock
					controller.Unlock()
				}
			}
			return http.StatusOK
		}
	} else {
		rw.WriteHeader( http.StatusBadRequest )
		_, _ = rw.Write( [ ]byte( "{\"error\":\"bad form\"}" ) )
		return http.StatusBadRequest
	}
}

func controllerButtonList(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	totalButtonList := make([]string,
		0,
		1)
	srv.Lock()
	for _, controller := range srv.controller {
		totalButtonList = append(totalButtonList,
			controller.ExtractButtonNameList()...)
	}
	srv.Unlock()
	content, _ := json.Marshal(totalButtonList)
	_, _ = rw.Write(content)
	return http.StatusOK
}

func controllerButtonChangeName(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// parse
	if err := request.ParseForm(); err == nil {
		// extract name
		name := common.ExtractFormValue(request,
			"name")
		newName := common.ExtractFormValue(request,
			"newName")

		// correct name
		for _, button := range []string{".button_on", ".button_off", ".button_up", ".button_down"} {
			name = strings.TrimSuffix(name,
				button)
		}
		name = strings.TrimRight(name,
			" ")
		for _, button := range []string{".button_on", ".button_off", ".button_up", ".button_down"} {
			newName = strings.TrimSuffix(newName,
				button)
		}
		newName = strings.TrimRight(newName,
			" ")

		if len(newName) <= 0 ||
			len( name ) <= 0 {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write( [ ]byte( "{\"error\":newName and/or name are empty\"}" ) )
			return http.StatusBadRequest
		} else {
			// lock
			srv.Lock()
			defer srv.Unlock()

			// look for light
			for _, controller := range srv.controller {
				// lock
				controller.Lock()

				// iterate lights
				for _, sw := range controller.Switch {
					isSelect := false
					if name == "" {
						isSelect = true
					} else {
						if sw.Name == name {
							isSelect = true
						}
					}
					if isSelect {
						go ChangeSwitchName( sw,
							controller.address,
							controller.apiKey,
							newName)
						if name != "" {
							break
						}
					}
				}

				// unlock
				controller.Unlock()
			}
		}
		return http.StatusOK
	} else {
		rw.WriteHeader( http.StatusBadRequest )
		_, _ = rw.Write( [ ]byte( "{\"error\":\"bad form\"}" ) )
		return http.StatusBadRequest
	}
}
