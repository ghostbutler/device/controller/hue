package service

const(
	DetectionRule =
`{
	"rule": {
		"name": "hue",
		"port": 80,
		"path": "/api/config",
		"method": "GET",
		"isHTTPS": false,
		"data": "",
		"possibleAnswer": [
			{
				"responseCode": 200,
				"isMustAnalyzeContent": true,
				"validContent": [
					{
						"expectedValue": "Im5hbWUiOiJQaGlsaXBzIGh1ZSI=",
						"isMustBeEqual": false
					}
				]
			}
		]
	},
	"controller": {
		"port": 16563,
		"path": "/api/v1/controller/add"
	}
}`
)