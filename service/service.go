package service

import (
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"gitlab.com/ghostbutler/tool/service/rule"
	"os"
	"sync"
)

const (
	VersionMajor = 1
	VersionMinor = 0
)

type Service struct {
	// service basis
	service *common.Service

	// key manager
	keyManager *keystore.KeyManager

	// configuration
	configuration *Configuration

	// scanner rule auto adder
	ruleAdderManager *rule.RuleManager

	// controllers list
	controller map[string]*Controller

	// build directory manager
	directoryManager *common.DirectoryManager

	// rabbit mq instance
	rabbitMQController *rabbit.Controller

	// mutex
	sync.Mutex
}

// build service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// allocate
	service := &Service{
		configuration: configuration,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceControllerHUE].Name),
		controller: make(map[string]*Controller),
	}

	// create api key directory
	if err := os.MkdirAll(configuration.ApiKeyDirectory,
		0766); err != nil {
		fmt.Println("couldn't create directory \""+
			configuration.ApiKeyDirectory+
			"\" :",
			err,
			"keys won't be saved")
	}

	// build RabbitMQ controller
	var err error
	if service.rabbitMQController, err = rabbit.BuildController(configuration.RabbitMQ.Hostname,
		configuration.RabbitMQ.Username,
		configuration.RabbitMQ.Password); err != nil {
		return nil, err
	}

	// create queues
	if _, err := service.rabbitMQController.Channel.QueueDeclare(rabbit.SensorQueueName,
		true,
		false,
		false,
		false,
		nil); err == nil {
	} else {
		return nil, err
	}

	// build directory manager
	service.directoryManager = common.BuildDirectoryManager(configuration.Directory.Hostname,
		service.keyManager)

	// build rule manager
	service.ruleAdderManager = rule.BuildRuleManager(service.configuration.Scanner.Hostname,
		service.keyManager,
		[]string{DetectionRule})

	// build service basis
	service.service = common.BuildService(listeningPort,
		common.ServiceControllerHUE,
		APIService,
		VersionMajor,
		VersionMinor,
		nil,
		[]device.CapabilityType{device.CapabilityTypeLight, device.CapabilityTypeGamepad},
		configuration.SecurityManager.Hostname,
		service)

	// ok
	return service, nil
}

// update
func (service *Service) Update() {
	// lock mutex
	service.Lock()
	defer service.Unlock()

	// iterate controller
	for key, controller := range service.controller {
		// is controller running?
		if !controller.isAlive {
			// notify
			fmt.Println("removing controller for",
				controller.address)

			// remove from map
			delete(service.controller,
				key)
		}
	}
}
